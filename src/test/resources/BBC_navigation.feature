Feature: Navigate to a chosen news category from the home page                                        //Scenario outline

  In order to get news from a specific category
  As a user
  I want the system to redirect me to a page with the news category of my chose

  Scenario Outline: User successfully re-directed to their chosen news category page

    Given user is on the BBC News home page
    When user navigates to a news "<category>" from the home page
    Then user should be re-directed to corresponding "<categoryName>" news page

    Examples:

      |                                  category                                                     |     categoryName      |
      |   /html[1]/body[1]/div[7]/header[1]/div[2]/div[1]/div[1]/nav[1]/ul[1]/li[2]/a[1]/span[1]      |        UK             |
      |   /html[1]/body[1]/div[7]/header[1]/div[2]/div[1]/div[1]/nav[1]/ul[1]/li[3]/a[1]/span[1]      |        World          |
      |   /html[1]/body[1]/div[7]/header[1]/div[2]/div[1]/div[1]/nav[1]/ul[1]/li[4]/a[1]/span[1]      |        Business       |
      |   /html[1]/body[1]/div[7]/header[1]/div[2]/div[1]/div[1]/nav[1]/ul[1]/li[5]/a[1]/span[1]      |        Politics       |


