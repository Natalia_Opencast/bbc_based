Feature: Sign in into account                   //Scenario with Background key word and positive/negative testing

  In order to Sign in into the account
  As a user
  I want the system to validate the input of user credentials

  Background:
    Given User navigates to BBC News website
    And User clicks on the Sign in option


  Scenario: Sing in into account with valid credentials

    Given User enters a valid email address
    And User enters a valid password
    When User clicks on the Sign in button
    Then User should be signed in successfully


  Scenario: Sign in into account with valid email and invalid password

    Given User enters a valid email address
    And User enters an invalid password
    When User clicks on the Sign in button
    Then Password isn't valid message is displayed to the user

  Scenario: Sign in into account with invalid email and valid password

    Given User enters an invalid email address
    And User enters a valid password
    When User clicks on the Sign in button
    Then Account doesn't exist message is displayed to the user


