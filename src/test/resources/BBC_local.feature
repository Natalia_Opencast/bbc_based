Feature: Check weather for a specified location                                          //Simple scenario

  In order to check local weather
  As a user
  I want the system to return weather for a specified location

  Scenario: User entered a location to get local weather

    Given user is on BBC News website
    And user clicks Weather menu
    And user enters specific location
    When user clicks on Search icon
    Then weather for specified location is shown


