package stepDefs;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import static utils.driverFactory.driver;
import static utils.driverFactory.navigationPage;

public class navigationSteps {

    @Given("user is on the BBC News home page")
    public void user_is_on_the_BBC_News_home_page() {
        driver.get("http:\\/\\/www.bbc.co.uk\\/news");
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
    }

    @When("user navigates to a news {string} from the home page")
    public void user_navigates_to_a_news_from_the_home_page(String category) throws Throwable {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(category))).click();
        Thread.sleep(2000);
    }

    @Then("user should be re-directed to corresponding {string} news page")
    public void user_should_be_re_directed_to_corresponding_news_page(String categoryName) {
        WebElement category = driver.findElement(By.xpath(navigationPage.selected));
        String actualCategory = category.getText();
        Assert.assertEquals(categoryName, actualCategory);

    }

}
