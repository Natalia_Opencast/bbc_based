package stepDefs;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import static utils.driverFactory.*;

public class localSteps {


    @Given("user is on BBC News website")
    public void user_is_on_BBC_News_website() {
        driver.get("http:\\/\\/www.bbc.co.uk");
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
    }

    @And("user clicks Weather menu")
    public void user_clicks_Weather_menu() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(localPage.weather))).click();
    }

    @And("user enters specific location")
    public void user_enters_specific_location() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(localPage.locatorInput))).sendKeys(localPage.testLocation);
    }

    @When("user clicks on Search icon")
    public void user_clicks_on_Search_icon() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(localPage.searchLocation))).click();
    }

    @Then("weather for specified location is shown")
    public void weather_for_specified_location_is_shown() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement setLocation = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(localPage.setWeatherLocation)));
        String locationActual = setLocation.getText();
        Assert.assertTrue (locationActual.contains(localPage.testLocation));
    }




}
