package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import pageObjects.navigationPage;
import pageObjects.loginPage;
import pageObjects.localPage;


public class driverFactory {
	
	public static WebDriver driver;
	public static loginPage loginPage;
	public static navigationPage navigationPage;
	public static localPage localPage;


	public WebDriver getChromeDriver() {

		try {


			String exePath = "src/test/drivers/chromedriverMac";
			System.setProperty("webdriver.chrome.driver", exePath );
			driver = new ChromeDriver();
			driver.manage().window().maximize();



		} catch (Exception e) { System.out.println("Unable to open a browser: "
				+ e.getMessage()); }
		finally {

			loginPage = PageFactory.initElements(driver, loginPage.class);
			navigationPage = PageFactory.initElements(driver, navigationPage.class);
			localPage = PageFactory.initElements(driver, localPage.class);
		}
		return driver;
	}


	public WebDriver getFirefoxDriver() {
		  
		  try {
		  
		  
		  driver = new FirefoxDriver();
		  driver.manage().window().maximize();

		  } catch (Exception e) { System.out.println("Unable to open browser: "
		  + e.getMessage()); } 
		  finally {

			  loginPage = PageFactory.initElements(driver, loginPage.class);
			  navigationPage = PageFactory.initElements(driver, navigationPage.class);
			  localPage = PageFactory.initElements(driver, localPage.class);
		  }
		  return driver;
	}

}
